# Teste Quality Assurance

Defina o conjunto ideal (eficaz e eficiente) de casos de teste de limite para um sistema que calcula os juros de depósito do termo.

## 1. Escopo e Estimativa

Leia primeiro todo o projeto, faça sua estimativa de horas do planejamento a ser realizado e envie um email com o título **[QA] Teste - Estimativa** para beawsome@waac.com.br

## 2. Entrega e Apresentação

Quando finalizar o teste, publique tudo no seu Github e envie um email com o título **[QA] Teste - Finalizado** com o link do projeto para beawsome@waac.com.br

## 3. O Teste

Uma calculadora informativa da taxa de juros para um depósito a prazo fixo precisa ser testada. Para um determinado valor de depósito e a idade do cliente, a calculadora mostra a taxa de juros anual.

As seguintes regras se aplicam:

1. O valor mínimo do depósito é $ 100.
2. O valor máximo do depósito é de US $ 10.000.
3. A taxa de juros anual depende do valor do depósito da seguinte forma:

|     VALOR DO DEPÓSITO       | TAXA DE JUROS ANUAL |
|-----------------------------|---------------------|
|US $ 100 - US $ 999          |1,0%                 |
|US $ 1.000 - US $ 4.999      |1,3%                 |
|US $ 5.000 - US $ 10.000     |1,5%                 |

4. Somente adultos (com 18 anos ou mais) são elegíveis para abrir uma conta de depósito a prazo.
5. Clientes com 60 anos ou mais têm uma taxa de juros fixa de 2%.

### Escopo tecnológico

Fique a vontade para elaborar a resposta com a tecnologia que mais se identificar, lembrando que você está lidando com um cenário de testes automatizados e recursivos.